import java.util.stream.IntStream;

public class Main {
    private static final String STAR = "*";
    private static final int START_INDEX = 1;
    private static final int END_INDEX = 10;

    public static void main(String[] args) {
        IntStream.rangeClosed(START_INDEX, END_INDEX)
                .forEach(iteration -> {
                    printStars(iteration);
                    printStars(iteration + 2);
                });
    }

    private static void printStars(int starsAmount) {
        IntStream.rangeClosed(START_INDEX, starsAmount)
                .forEach(iteration -> System.out.print(STAR));
        System.out.println();
    }
}